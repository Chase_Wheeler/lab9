#include <stdio.h>
#include <string.h>
#include "md5.h"
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *fp;
    FILE *dest;
    fp = fopen(argv[1], "r");
    dest = fopen(argv[2], "w");
    char *hash;
    int lineCount;
    char line[225];

    //error messages
    if(argv[1] == NULL)
    {
        printf("Select a file to read from.");
    }

    if(argv[2] == NULL)
    {
        printf("Select a file to write to.");
    }

    //take in lines from source file
    while(fgets(line, 225, fp) != NULL)
    {

        //trim new lines
        char *nl = strchr (line, '\n');
        if (nl) *nl = '\0';

        //find length
        for (int i = 0; i <= line['\0']; i++)
        {
            lineCount++;
        }
        
        //hash & write to file
        hash = md5(line, lineCount);
        fprintf(dest, "%s", hash);
        
    }
    
    
    printf("Done.\n");

    fclose(fp);
    fclose(dest);
    exit(1);
    
}