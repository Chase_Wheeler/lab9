#include <stdio.h>
#include <math.h>
#include <string.h>

int main(int argc, char *argv[])
{  
    FILE *fp;
    fp = fopen(argv[1], "r");
    char line[225];
    char * stem;

    if(!*argv[1])
    {
        printf("Select a file to read from.");
    }

    if (fp == NULL)
    {
        printf("Can't open this file.\n");
        return 0;
    }

    while(fgets(line, 225, fp) != NULL)
    {

        stem = strstr(line, argv[2]);

        if(stem)
        {
            printf("%s", line);
        }

    }

    printf("\n");
    return 0;
}